package com.xiaoming.web.api;

import com.xiaoming.annotation.Autowired;
import com.xiaoming.annotation.Controller;
import com.xiaoming.annotation.RequestMapping;
import com.xiaoming.annotation.Security;
import com.xiaoming.web.entiry.User;
import com.xiaoming.web.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/user")
@Security(names = {"zhangsan","lisi"})
public class UserController {
    @Autowired
    private UserService userService;

    @Security(names = {"zhangsan"})
    @RequestMapping("/get")
    public void getUser(String userId, HttpServletResponse response, HttpServletRequest request) throws IOException {
        User user = userService.getUser(userId);
        System.out.println(user);
        response.getWriter().write("200 Success");
    }
}
