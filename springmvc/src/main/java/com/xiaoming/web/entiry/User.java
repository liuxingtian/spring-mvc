package com.xiaoming.web.entiry;

public class User {
    private String name;
    private int age;
    private String userId;

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }
}
