package com.xiaoming.web.service;

import com.xiaoming.web.entiry.User;

public interface UserService {
    public User getUser(String userId);
}
