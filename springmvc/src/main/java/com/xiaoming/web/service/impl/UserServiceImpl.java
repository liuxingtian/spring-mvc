package com.xiaoming.web.service.impl;

import com.xiaoming.annotation.Service;
import com.xiaoming.web.entiry.User;
import com.xiaoming.web.service.UserService;

import java.util.Random;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User getUser(String userId) {
        User user = new User();
        user.setName("张三" + new Random().nextInt(10) + 1);
        user.setAge(new Random().nextInt(100) + 1);
        user.setUserId(userId);
        return user;
    }
}
