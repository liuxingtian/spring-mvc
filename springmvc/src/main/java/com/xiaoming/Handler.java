package com.xiaoming;

import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

public class Handler {
    private Object controller;
    private Method method;
    private Map<String, Integer> paramIndexMap;
    private Pattern pattern;
    private List<String> securities;

    public Handler(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMap = new HashMap<>();
    }

    public List<String> getSecurities() {
        return securities;
    }

    public Map<String, Integer> getParamIndexMap() {
        return paramIndexMap;
    }

    public Method getMethod() {
        return method;
    }

    public Object getController() {
        return controller;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public void setParamIndexMap(Map<String, Integer> paramIndexMap) {
        this.paramIndexMap = paramIndexMap;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public void setSecurities(List<String> securities) {
        this.securities = securities;
    }
}
