# 说明

当前目录下有两个项目，springmvc 和 sssdemo，springmvc完成了作业1，实现@Security，sssdemo完成作业2，实现登录等功能；

## springmvc

在web.api有一个UserController

```java
@Controller
@RequestMapping("/user")
@Security(names = {"zhangsan","lisi"})
public class UserController {
    @Autowired
    private UserService userService;

    @Security(names = {"zhangsan"})
    @RequestMapping("/get")
    public void getUser(String userId, HttpServletResponse response, HttpServletRequest request) throws IOException {
        User user = userService.getUser(userId);
        response.getWriter().write("200 Success");
    }
}
```

这个项目引入了一个 spring-core 解决反射获取 Parameter 参数名称为 arg0的问题

**如果能够正确访问，返回 "200 Success"，没有权限返回 "403 No Permission";**

复制以下地址（Markdown点击跳转好像有点问题，建议复制）

**有权限：**<http://localhost:8000/user/get?userId=1&&userName=zhangsan>
**没权限：**<http://localhost:8000/user/get?userId=1&&userName=lisi>

## ssdemo

**首页地址：**<http://127.0.0.1:8080/>
**列表页：**<http://127.0.0.1:8080/resume>

其中列表页的新增和编辑在一个表单里面，可以通过文字样式标识当前是新增还是编辑
