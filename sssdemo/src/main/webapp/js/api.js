// 查询整个列表
var resumesList = [];
function findAll() {
    $.ajax({
        url: '/resume/list',
        type: 'POST',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        success: function (data) {
            //生成表格数据
            var tr = '<tr>' +
                '<th>ID</th>' +
                '<th>姓名</th>' +
                '<th>地址</th>' +
                '<th>电话</th>' +
                '<th>操作</th>' +
                '</tr>';
            resumesList = data;
            $("#list").html("");
            $.each(data, function (index, item) {
                $('tr + <tr id="tr' + item.id + '"><td>'
                    + item.id + '</td><td>'
                    + item.name + '</td><td>'
                    + item.address + '</td><td>'
                    + item.phone + '</td><td><input type="button" value="修改"  onclick="editResume('+item.id+')"/>'
                    +'<input type="button" value="删除" onclick="deleteResume('+item.id+')"/></td></tr>').appendTo($("#list"));
            });
        }
    })
}
function save (data){
    $.ajax({
        url: '/resume/save',
        type: 'POST',
        data:JSON.stringify(data),
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        success: function (data) {
            findAll();
        }
    })
}
function del(id) {
    $.ajax({
        url: '/resume/delete',
        type: 'POST',
        data:JSON.stringify({
            "id":id,
        }),
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        success: function (data) {
            findAll();
        }
    })
}