<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>简历</title>
</head>
<body>
<p>登录成功</p>
<table id="list" border="1" cellspacing="0" cellpadding="0"></table>

<p style="margin-top:50px">
    <button onclick="addResume()" n>新增</button>
</p>
<p>
    提交表单用于
    <span style="color:blue" id="form-active">新增</span>/
    <span  id="form-status">编辑</span>
</p>
<div>
    <input type="hidden" id="id" />
    姓名：<input type="text" id="name" />
    <br>
    地址：<input type="text" id="address"/>
    <br>
    电话：<input type="text" id="phone"/>
    <br>
    <input type="button" id="btnSave" onclick="saveResume()" value="提交"/>
</div>

</body>
</html>
<script src="/js/jquery.min.js"></script>
<script src="/js/api.js"></script>
<script>
$(function(){
    findAll();
    formStatusChange();
})
function saveResume() {
    var data = {
        id:$("#id").val(),
        name:$("#name").val(),
        address:$("#address").val(),
        phone:$("#phone").val()
    }
    save(data);
}
function editResume(id){
    var item = resumesList.find(resume=>resume.id === id);
    $("#id").val(item.id)
    $("#name").val(item.name)
    $("#address").val(item.address)
    $("#phone").val(item.phone)
    formStatusChange()
}
function addResume(){
    $("#id").val('')
    $("#name").val('')
    $("#address").val('')
    $("#phone").val('')
    formStatusChange()
}
function formStatusChange(){
    var value = $("#id").val()
    if(value.length === 0){
        $("#form-active").text("新增")
        $("#form-status").text("编辑")
    }else{
        $("#form-active").text("编辑")
        $("#form-status").text("新增")
    }
}
function deleteResume(id){
    del(id);
}
</script>