package com.xiaoming.api;

import com.xiaoming.pojo.Resume;
import com.xiaoming.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/resume")
public class ResumeController {
    @Autowired
    private ResumeService resumeService;

    @RequestMapping("/list")
    public @ResponseBody List<Resume> queryAll() {
        return resumeService.queryAll();
    }

    @RequestMapping("/save")
    public @ResponseBody void saveResume(@RequestBody Resume r) {
        resumeService.saveResume(r);
    }

    @RequestMapping("/delete")
    public @ResponseBody void deleteResume(@RequestBody Resume r) {
        resumeService.deleteResume(r.getId());
    }
}
