package com.xiaoming.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class ViewController {

    @RequestMapping("/login")
    public void login(HttpServletRequest req, HttpServletResponse resp, String username, String pwd) throws IOException {
        if("admin".equals(username)&&"admin".equals(pwd)){
            HttpSession session = req.getSession();
            session.setAttribute("user","admin");
            System.out.println("用户已登陆");
        }
        resp.sendRedirect("resume");
    }

    @RequestMapping("/resume")
    public ModelAndView jumpToList(){
        return new ModelAndView("resume");
    }
}
