package com.xiaoming.service;

import com.xiaoming.pojo.Resume;

import java.util.List;

public interface ResumeService {

    public List<Resume> queryAll();
    public Resume saveResume(Resume r);
    public void deleteResume(long id);
}
