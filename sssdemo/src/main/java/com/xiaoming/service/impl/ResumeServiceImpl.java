package com.xiaoming.service.impl;

import com.xiaoming.dao.ResumeDao;
import com.xiaoming.pojo.Resume;
import com.xiaoming.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service("resumeService")
public class ResumeServiceImpl implements ResumeService {
    @Autowired
    private ResumeDao resumeDao;


    @Override
    public List<Resume> queryAll() {
        return resumeDao.findAll();
    }

    @Override
    public Resume saveResume(Resume r) {
        return resumeDao.save(r);
    }

    @Override
    public void deleteResume(long id) {
        resumeDao.deleteById(id);
    }
}
